#define X11BACKEND_HPP
#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <X11/Xutil.h>
#include <string>
#include <vector>
#include "types.hpp"
#ifndef MONITORING_HPP
#include "monitoring.hpp"
#endif
#include "xbm.hpp"

typedef struct
{
	const char* color;
	unsigned int icon;
} tile;

class X11Backend : public DesktopBackend
{
	private:
		Display *display;
		Window panelW;
		XFontStruct *font;
		int screen;
		int width;
		int height;
		int numOfTiles;
		std::vector<xbm*> icons;
		std::vector<tile> tiles;
		unsigned int tileCounter = 0;



	public:
		X11Backend();
		~X11Backend();
		int getWidth();
		int getHeight();
		void createWindow(int width, int height);
		void handleEvent(DesktopEvent *e);
		DesktopEvent* getNextEvent();
		unsigned int loadIcon(const char* filename);
		unsigned int reserveTiles(unsigned int howMuch);
		void paint(PaintEvent* e);
		void paint(unsigned int tileNum, unsigned int iconNum, const char* colorString);
		void repaint();
		void paintTime(TimeEvent *e);
};

