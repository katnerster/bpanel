#include <iostream>
#include <thread>
#include "x11backend.hpp"
#include "monitoring.hpp"
#include <vector>


int main(int argc, char **argv)
{

	X11Backend b;
	b.createWindow(16,800);
	EventQueue eventQueue;
	std::vector<Monitor*> monitors;
	monitors.push_back(new WorkspacesMonitor(eventQueue, b));
	monitors.push_back(new MPDMonitor(eventQueue, b));
	monitors.push_back(new ClockMonitor(eventQueue));
	HwMonitor hm(eventQueue, b, &monitors);
	std::thread hwThread(&Monitor::witam, hm);
	while (true) {
		DesktopEvent* event = eventQueue.dequeueEvent();
		b.handleEvent(event);
		if ( event != NULL ) delete event;
	}
	return 0;
}
