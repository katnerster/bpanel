CXX = g++
CXXFLAGS = -g -Wall -std=c++11
LDFLAGS = -lX11 -lmpdclient -lpthread `pkg-config --libs cairo`

all: types.o x11backend.o monitoring.o xbm.o main.o
	rm -f bPanel
	$(CXX) $(CXXFLAGS) $(LDFLAGS) types.o main.o x11backend.o monitoring.o xbm.o -o bPanel

x11backend.o: x11backend.cpp
	$(CXX) $(CXXFLAGS) -c -lX11 -lpthread `pkg-config --libs cairo` x11backend.cpp -o x11backend.o

monitoring.o: monitoring.cpp
	$(CXX) $(CXXFLAGS) -c -lX11 -lmpdclient -lpthread `pkg-config --libs cairo` monitoring.cpp -o monitoring.o

types.o: types.cpp
	$(CXX) $(CXXFLAGS) -c -lX11 -lmpdclient -lpthread `pkg-config --libs cairo` types.cpp -o types.o

xbm.o: xbm.cpp
	$(CXX) $(CXXFLAGS) -c -lX11 -lmpdclient -lpthread `pkg-config --libs cairo` xbm.cpp -o xbm.o

main.o: main.cpp
	$(CXX) $(CXXFLAGS) -c -lX11 -lmpdclient -lpthread `pkg-config --libs cairo` main.cpp -o main.o

clean:
	rm *.o
