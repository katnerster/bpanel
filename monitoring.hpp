#ifndef MONITORING_HPP
#define MONITORING_HPP
#include <queue>
#include "types.hpp"
#include <condition_variable>
#include <iostream>
#include <vector>
#include <fstream>
#include <thread>
#include <mpd/client.h>
class EventQueue
{
	private:
		std::queue<DesktopEvent*> events;
		std::condition_variable cond;
		std::mutex eMutex;

	public:
		EventQueue() {};
		~EventQueue() {};
		void enqueueEvent(DesktopEvent* e);
		DesktopEvent* dequeueEvent();
};

class WorkspacesEvent :public DesktopEvent
{
	private:
		deskInfo info;

	public:
 	   	WorkspacesEvent(deskInfo info) : DesktopEvent(EVENT_WORKSPACE)
	{
		this->info = info;
	}
	~WorkspacesEvent();
	deskInfo getInfo()
	{
		return info;
	}
};

class TimeEvent : public DesktopEvent
{
	private:
		std::string hour;
		std::string minute;

	public:
		TimeEvent(std::string hour, std::string minute) : DesktopEvent(EVENT_TIME)
	{
		this->hour = hour;
		this->minute = minute;
	}
	~TimeEvent();
	std::string getHour()
	{
		return hour;
	}
	std::string getMinute()
	{
		return minute;
	}
};

class MPDEvent : public DesktopEvent
{
	private:
		mpdInfo info;

	public:
		MPDEvent(mpdInfo &info) : DesktopEvent(EVENT_MPD)
	{
		this->info = info;
	}
		~MPDEvent();
	mpdInfo getInfo()
	{
		return info;
	}
	
};

class Monitor
{
	public:
		Monitor() {};
		~Monitor() {};
		virtual void witam() {};
		virtual void clicked(int tileNum, int button) {};
};

class MPDMonitor : public Monitor
{
	private:
		EventQueue *e;
		std::thread *th;
		struct mpd_connection *connection;
		unsigned int tile;
		unsigned int playIcon, pauseIcon, stopIcon;

	public:
		MPDMonitor(EventQueue &eventQueue, DesktopBackend &backend);
		~MPDMonitor();
		void witam();
		mpdInfo getMpdInfo();
		void clicked(int tileNum, int button);
};

class WorkspacesMonitor : public Monitor
{
	private:
		EventQueue *e;
		unsigned int firstTile;
		unsigned int desktopIcon, monocleIcon, tiledIcon;
		std::thread *th;

	public:
		WorkspacesMonitor(EventQueue &eventQueue, DesktopBackend &backend);
		~WorkspacesMonitor();
		void witam();
		void clicked(int tileNum, int button) ;
};

class HwMonitor : public Monitor
{
	private:
	   EventQueue *e;
	   DesktopBackend* backend;
	   std::vector<Monitor*> *monitors;

	public:
	   HwMonitor(EventQueue &eventQueue, DesktopBackend &backend, std::vector<Monitor*> *monitors);
	   ~HwMonitor();
	   void witam();
};

class ClockMonitor : public Monitor
{
	private:
	   EventQueue *e;
	   std::thread *th;

	public:
	   ClockMonitor(EventQueue &eventQueue);
	   ~ClockMonitor();
	   void witam();
		void clicked(int tileNum, int button);
};
#endif
