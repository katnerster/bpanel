#include <string>
#include <vector>
#include <string>
#include <iostream>
#ifndef TYPES_HPP
#define TYPES_HPP

enum
{
	EVENT_WORKSPACE = 1,
	EVENT_MOUSECLICK = 2,
	EVENT_TIME = 3,
	EVENT_MPD = 4,
	EVENT_RAISE_WINDOW = 5,
	EVENT_EXPOSE = 6
};

enum 
{
	E_RESERVE_TILES = 13,
	E_PAINT_TILE = 14
};

class DesktopEvent
{
	private:
		std::string uid;
		int type;

	public:
		DesktopEvent(int type) { this->type = type; this->uid = "uid"; }
		virtual ~DesktopEvent() { }
		void setType(int type) { this->type = type; }
		std::string getUID() { return uid; }
		int getType() { return type; } 
		virtual int getX() {};
		virtual int getY() {};
		virtual int getButton() {return 0;};

};

class ReserveEvent : public DesktopEvent
{
	private:
		int id;
		int howMuch;
	public:
		ReserveEvent(int id, int howMuch) : DesktopEvent(E_RESERVE_TILES) 
	    {
			this->id = id;
			this->howMuch = howMuch;
		};
		~ReserveEvent();
		int getId()
		{
			return id;
		}
		int getHowMuch()
		{
			return howMuch;
		}
};

class PaintEvent : public DesktopEvent
{
	private:
		int id;
		int which;
		int iconID;
		const char *color;
	public:
		PaintEvent(int id, int which, int iconID, const char* color) : DesktopEvent(E_PAINT_TILE) 
	    {
			this->id = id;
			this->which = which;
			this->iconID = iconID;
			this->color = color;
		};
		~PaintEvent();
		int getId()
		{
			return id;
		}
		int getWhich()
		{
			return which;
		}
		int getIconID()
		{
			return iconID;
		}
		const char *getColor()
		{
			return color;
		}
};

class MouseClickEvent : public DesktopEvent
{
	//private:
	
	public:
		MouseClickEvent(int x, int y, int button) : DesktopEvent(EVENT_MOUSECLICK)
	{
		this -> x = x;
		this -> y = y;
		this -> button = button;
	};
		int x, y, button;
		int getX()
		{
			return this->x;
		};
		int getY()
		{
			return this->y;
		};
		int getButton()
		{
			return this->button;
		};
};

class RaiseWindowEvent : public DesktopEvent
{
	public:
		RaiseWindowEvent() : DesktopEvent(EVENT_RAISE_WINDOW)
	{};
};

typedef struct 
{
	std::string monitor;
	std::vector<int> desktops;
	std::string mode;
} deskInfo;

typedef struct
{
	int state;
	std::string title;
	std::string artist;
	std::string album;
	int duration;

} mpdInfo;

class DesktopBackend
{
	public:
		virtual ~DesktopBackend();
		virtual int getWidth() {return -1;};
		virtual int getHeight() {return -1;};
		virtual unsigned int loadIcon(const char* filename) { return 128;}; 
		virtual unsigned int reserveTiles(unsigned int howMuch) {return 0;};
		virtual DesktopEvent* getNextEvent() { return nullptr; };
};
#endif
