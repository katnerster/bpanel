#include "monitoring.hpp"

#include <stdio.h>
#include <chrono>
#include <iostream>
#include <thread>
#include <string>
#include <vector>
#include <fstream>

using namespace std;

void EventQueue::enqueueEvent(DesktopEvent* e) {
	lock_guard<mutex> lock(eMutex);
	events.push(e);
	cond.notify_all();
}

DesktopEvent* EventQueue::dequeueEvent() {
	unique_lock<mutex> lock(eMutex);
	while(events.size() == 0) { cond.wait(lock); }

	DesktopEvent* event = events.front();
	events.pop();
	return event;
}

WorkspacesEvent::~WorkspacesEvent()
{}

TimeEvent::~TimeEvent()
{}

MPDEvent::~MPDEvent()
{}

deskInfo parseInfo (std::vector<string> source)
{
	deskInfo result;
	for (std::vector<std::string>::iterator tmp = source.begin(); tmp != source.end(); tmp++)
	{
		std::string str = *tmp;
		if(str.substr(0,2) == std::string("WM"))
		{
			result.monitor = str.substr(2,str.length());
		} else if (str.substr(0,1) == std::string("f"))
		{
			result.desktops.push_back(0);
		} else if (str.substr(0,1) == std::string("o"))
		{
			result.desktops.push_back(1);
		} else if (str.substr(0,1) == std::string("O"))
		{
			result.desktops.push_back(2);
		} else if (str.substr(0,1) == std::string("u"))
		{
			result.desktops.push_back(3);
		} else if (str.substr(0,1) == std::string("F"))
		{
			result.desktops.push_back(4);
		} else if (str.substr(0,1) == std::string("L"))
		{
			if (str.substr(1,str.length()-1) == std::string("monocle"))
			{
				result.mode = std::string("M");
			} else if (str.substr(1,str.length()-1) == std::string("tiled"))
			{
				result.mode = std::string("T");
			}
		}
	}
	return result;
}
deskInfo parse(const std::string &source)
{
	std::vector<string> tmp;
	size_t prev = 0;
	size_t next = 0;
	while((next = source.find_first_of(":", prev)) != std::string::npos)
	{
		if (next - prev != 0)
		{
			tmp.push_back(source.substr(prev, next - prev));
		}
		prev = next + 1;
	}
	if (prev < source.size())
	{
		tmp.push_back(source.substr(prev));
	}
	return parseInfo(tmp);
}
			

WorkspacesMonitor::WorkspacesMonitor(EventQueue &eventQueue, DesktopBackend &backend)
{
	this->e = &eventQueue;
	firstTile = backend.reserveTiles(11);
	desktopIcon = backend.loadIcon("/tmp/cpu.xbm");
	monocleIcon = backend.loadIcon("/tmp/monocle.xbm");
	tiledIcon = backend.loadIcon("/tmp/tiled.xbm");
	th = new std::thread(&WorkspacesMonitor::witam, this);
	th->detach();

}

void WorkspacesMonitor::witam()
{
	FILE *in;
	if(!(in = popen("bspc control --subscribe", "r"))){
		return;
	}
	const char* tab[] = {"#0000ff", "#FF00FF", "#ffff00", "#138665", "#999999"};

	std::string tmpString;
	char buff[1024];
		while(true)
		{
			//std::getline(in, tmpString);
			fgets(buff, sizeof(buff), in);
			tmpString =  std::string(buff);
			std::cout << tmpString << std::endl;
			deskInfo info = parse(tmpString.substr(0,tmpString.length()-1));
			for (int i=0;i<10;i++)
			{
				e->enqueueEvent((DesktopEvent*) new PaintEvent(1,i,desktopIcon,tab[info.desktops[i]]));
			}
			if (info.mode == std::string("T"))
			{
				e->enqueueEvent((DesktopEvent*) new PaintEvent(1,10,tiledIcon,"#FFFFFF"));
			} else
			{
				e->enqueueEvent((DesktopEvent*) new PaintEvent(1,10,monocleIcon, "#FFFFFF"));
			}

			//e->enqueueEvent((DesktopEvent*) new PaintEvent(1,0,0, "#FACABD"));
		}
}
WorkspacesMonitor::~WorkspacesMonitor()
{
	//(*WorkspacesMonitor::fifo).close();
}
void WorkspacesMonitor::clicked(int tileNum, int button)
{
	if (tileNum>=0 && tileNum < 10)
	{
		if (button == 1)
		{
			std::string command = "bspc desktop -f ^" + std::to_string(tileNum+1);
			system(command.c_str());
		} else if (button == 4)
		{
			system("bspc desktop -f prev");
		} else if (button == 5)
		{
			system("bspc desktop -f next");
		}
		e->enqueueEvent((DesktopEvent*) new RaiseWindowEvent());
	}
}

HwMonitor::HwMonitor(EventQueue &eventQueue, DesktopBackend &backend, std::vector<Monitor*> *monitors)
{
	this->e = &eventQueue;
	this->backend = &backend;
	this->monitors = monitors;
}

HwMonitor::~HwMonitor()
{
}

void HwMonitor::witam()
{
		while(true) {
		DesktopEvent* event = backend->getNextEvent();
		if (event != nullptr) {
			if (event->getType() == EVENT_MOUSECLICK)
			{
				for (Monitor* monitor : (*monitors))
				{
					MouseClickEvent *ev = (MouseClickEvent*)event;

				monitor->clicked(ev->y/16, ev->button);
				//e->enqueueEvent((DesktopEvent*) new PaintEvent(1,0,0,0));
				}
				// tutaj obsługa
			} else
			{
				e->enqueueEvent(event);
			}
		} else {
			std::this_thread::sleep_for(std::chrono::milliseconds(5));
		}
	}
};

ClockMonitor::ClockMonitor(EventQueue &eventQueue)
{
	this->e = &eventQueue;
	th = new std::thread(&ClockMonitor::witam, this);
}

ClockMonitor::~ClockMonitor()
{
}

std::string getHour() {
	time_t t = time(nullptr);
	char timestamp[3];
	strftime(timestamp, 3, "%H", localtime(&t));
	return string(timestamp);
}

std::string getMinute() {
	time_t t = time(nullptr);
	char timestamp[3];
	strftime(timestamp, 3, "%M", localtime(&t));
	return string(timestamp);
}

void ClockMonitor::witam()
{
		while(true) {
			e->enqueueEvent((DesktopEvent*) new TimeEvent(getHour(), getMinute()));
			std::this_thread::sleep_for(std::chrono::milliseconds(1000));
		}
};

void ClockMonitor::clicked(int tileNum, int button)
{
}

MPDMonitor::MPDMonitor(EventQueue &eventQueue, DesktopBackend &backend)
{
	this->e = &eventQueue;
	tile = backend.reserveTiles(1);
	playIcon = backend.loadIcon("/tmp/play.xbm");
	pauseIcon = backend.loadIcon("/tmp/pause.xbm");
	stopIcon = backend.loadIcon("/tmp/stop.xbm");
	th = new std::thread(&MPDMonitor::witam, this);
}
void MPDMonitor::clicked(int tileNum, int button)
{
}

MPDMonitor::~MPDMonitor()
{
}

mpdInfo MPDMonitor::getMpdInfo()
{
	struct mpd_status *status;
	struct mpd_song *song;
	mpdInfo info;
	status = mpd_run_status(connection);
	info.state = mpd_status_get_state(status);
	if (info.state == MPD_STATE_PLAY || info.state == MPD_STATE_PAUSE)
	{
		song = mpd_run_current_song(connection);
		if (song != NULL)
		{
			info.title = mpd_song_get_tag(song, MPD_TAG_TITLE, 0) ? : "";
			info.artist = mpd_song_get_tag(song, MPD_TAG_ARTIST, 0) ? : "";
			info.album = mpd_song_get_tag(song, MPD_TAG_ALBUM, 0) ? : "";
			info.duration = mpd_song_get_duration(song);
			mpd_song_free(song);
		} else
		{
			info.title = info.artist = info.album = "";
			info.duration = 0;
		}
	}
	mpd_status_free(status);
	return info;
}

void MPDMonitor::witam()
{
	connection = mpd_connection_new(NULL, 0, 30000);

	if (mpd_connection_get_error(connection) == MPD_ERROR_SUCCESS)
	{
		mpdInfo info = getMpdInfo();
		if(info.state == MPD_STATE_PLAY)
		{
			e->enqueueEvent((DesktopEvent*) new PaintEvent(1,tile,playIcon,"#FFFFFF"));
		} else if (info.state == MPD_STATE_PAUSE)
		{
			e->enqueueEvent((DesktopEvent*) new PaintEvent(1,tile,pauseIcon,"#FFFFFF"));
		} else if (info.state == MPD_STATE_STOP)
		{
			e->enqueueEvent((DesktopEvent*) new PaintEvent(1,tile,stopIcon,"#FFFFFF"));
		}
		while(true) 
		{
			mpd_idle idle = mpd_run_idle(connection);
			mpdInfo info = getMpdInfo();
			if(info.state == MPD_STATE_PLAY)
			{
				e->enqueueEvent((DesktopEvent*) new PaintEvent(1,tile,playIcon,"#FFFFFF"));
			} else if (info.state == MPD_STATE_PAUSE)
			{
				e->enqueueEvent((DesktopEvent*) new PaintEvent(1,tile,pauseIcon,"#FFFFFF"));
			} else if (info.state == MPD_STATE_STOP)
			{
				e->enqueueEvent((DesktopEvent*) new PaintEvent(1,tile,stopIcon,"#FFFFFF"));
			}
		}
	}
};
