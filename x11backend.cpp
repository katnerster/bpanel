#include <iostream>
#include <cmath>
#include <exception>
#include <string>
#include <cstdlib>
#include "x11backend.hpp"


X11Backend::X11Backend()
{
	XInitThreads();
	display = XOpenDisplay(NULL);
	if (!display)
		std::cout <<"BŁĄD!";
		//throw new exception();
	screen = DefaultScreen(display);
	numOfTiles = getHeight()/16;
	tiles.reserve(numOfTiles);
	font = XLoadQueryFont(display, "8x13");
}
X11Backend::~X11Backend()
{
	XCloseDisplay(display);
}

int X11Backend::getHeight()
{
	return DisplayHeight(display, screen);
}
int X11Backend::getWidth()
{
	return DisplayWidth(display, screen);
}

void X11Backend::createWindow(int width, int height)
{
	this->width = width;
	this->height = height;
	panelW = XCreateSimpleWindow(display, DefaultRootWindow(display)
			, 0, 0, width, height, 0, BlackPixel(display, screen), 
			BlackPixel(display,screen));
	XSelectInput(display, panelW, ExposureMask | ButtonPressMask);
	Atom tmp = XInternAtom(display, "_NET_WM_WINDOW_TYPE_DOCK", False);
	XChangeProperty(display, panelW, 
			XInternAtom(display, "_NET_WM_WINDOW_TYPE", False), 
			XA_ATOM, 32, PropModeReplace, (const unsigned char*)&tmp, 1);
	XFlush(display);
	XMapWindow(display, panelW);
	XSync(display, 0);
}


unsigned int X11Backend::loadIcon(const char* filename)
{
	xbm* tmp = new xbm(filename, display, panelW);
	icons.push_back(tmp);
	if (icons.size() == 1)
	{
		return 0;
	} else
	{
		return icons.size()-1;
	}
}

unsigned int X11Backend::reserveTiles(unsigned int howMuch)
{
	unsigned int tmp = tileCounter;
	tileCounter += howMuch;
	return tmp;

}

void X11Backend::paint(PaintEvent* e)
{
	xbm* icon = icons[e->getIconID()];
	XColor color;
	XParseColor(display, DefaultColormap(display, screen), e->getColor(), &color);
	XAllocColor(display, DefaultColormap(display, screen), &color);
	XSetForeground(display, DefaultGC(display, screen), color.pixel);
	XSetBackground(display, DefaultGC(display, screen), BlackPixel(display, screen));
	int y = 16 * e->getWhich() + (16.0/2.0-icon->h()/2.0);
	XClearArea(display, panelW, 0, y, 16, 16, false);
	XCopyPlane(display, icon->p(), panelW, DefaultGC(display, screen),
			0,0,16,16,width/2.0-icon->w()/2.0, y, 1);
	tile t;
	t.icon = e->getIconID();
	t.color = e->getColor();
	tiles[e->getWhich()] = t;

}

void X11Backend::paint(unsigned int tileNum, unsigned int iconNum, const char* colorString)
{
	xbm* icon = icons[iconNum];
	XColor color;
	XParseColor(display, DefaultColormap(display, screen), colorString, &color);
	XAllocColor(display, DefaultColormap(display, screen), &color);
	XSetForeground(display, DefaultGC(display, screen), color.pixel);
	XSetBackground(display, DefaultGC(display, screen), BlackPixel(display, screen));
	int y = 16 * tileNum + (16.0/2.0-icon->h()/2.0);
	XClearArea(display, panelW, 0, y, 16, 16, false);
	XCopyPlane(display, icon->p(), panelW, DefaultGC(display, screen),
			0,0,16,16,width/2.0-icon->w()/2.0, y, 1);
}
void X11Backend::handleEvent(DesktopEvent *e)
{

	switch(e->getType())
	{
		case E_PAINT_TILE:
			paint((PaintEvent*)e);
			break;
		case EVENT_RAISE_WINDOW:
			XRaiseWindow(display, panelW);
			break;
		case EVENT_TIME:
			paintTime((TimeEvent*)e);
			break;
	}
}

void X11Backend::repaint()
{
	for(int i = 0; i < tileCounter; i++)
	{
		paint(i, tiles[i].icon, tiles[i].color);
	}
}

void X11Backend::paintTime(TimeEvent *e)
{
	std::string hour = e->getHour();
	std::string minute = e->getMinute();
	XSetForeground(display, DefaultGC(display, screen), WhitePixel(display, screen));
	int tw = XTextWidth(this->font, "0", 1);
	int th = font->ascent;
	XClearArea(display, panelW, 0,height-4*th,width,4*th,false);
	XDrawString(display, panelW, DefaultGC(display, screen), width/2.0-tw/2.0+1, height - 3*th, hour.substr(0,1).c_str(), 1);
	XDrawString(display, panelW, DefaultGC(display, screen), width/2.0-tw/2.0+1, height - 2*th, hour.substr(1,2).c_str(), 1);
	XDrawString(display, panelW, DefaultGC(display, screen), width/2.0-tw/2.0+1, height - 1*th, minute.substr(0,1).c_str(), 1);
	XDrawString(display, panelW, DefaultGC(display, screen), width/2.0-tw/2.0+1, height, minute.substr(1,2).c_str(), 1);

}

DesktopEvent* X11Backend::getNextEvent()
{
	if(XPending(display) == 0) return nullptr;
	XEvent e;
	XNextEvent(display, &e);
	DesktopEvent* event = nullptr;
	switch(e.type) {
    	case ButtonPress: 
        	event = (DesktopEvent*)new MouseClickEvent(e.xbutton.x, e.xbutton.y, e.xbutton.button); break;
		case Expose:
			repaint(); break;
    	default: event = new DesktopEvent(255); break;
	}   
	return event;
}

